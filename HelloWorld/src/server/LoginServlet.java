package server;

import java.io.IOException;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.UserBean;

public class LoginServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = -174546386041946579L;
	private String name;
	private String pass;
	public void doGet(HttpServletRequest request,HttpServletResponse response) throws IOException,ServletException{
		name = (String)request.getParameter("username");
		pass = (String)request.getParameter("userpass");
		UserBean u = new UserBean();
		boolean b= u.validate(name, pass);
		String forward;
		if (b) { 
			HttpSession session = request.getSession(true);
			session.setAttribute("username", name);
	        forward = "success.jsp";
		}else{
			forward = "failure.jsp";
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher(forward);
		dispatcher.forward(request, response);
	}

	public void doPost(HttpServletRequest request,HttpServletResponse response) throws IOException,ServletException{doGet(request,response);
	}

	public LoginServlet() {
		// TODO 自动生成的构造函数存根
	}

	public static void main(String[] args) {
		// TODO 自动生成的方法存根

	}

}
